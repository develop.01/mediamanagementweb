﻿async function ajaxGet(url, callback = null) {
   
    var response = await fetch(url, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            Authorization: 'Bearer ' + constructionApiToken
        },
    });
    var json = await response.json();
    if (!response.ok) {
        if (json.statusCode === 401) {
            await getTokenAsync();
            return ajaxGet(url, callback);
        }
       
        if (json) {
            modalWaring(json.messageTh || json.message || 'เกิดข้อผิดพลาดในระบบ กรุณาติดต่อผู้ดูแลระบบ');
        }
        if (callback) {
            return callback(false);
        }
        return false;
    }
    if (callback) {
        return callback(json);
    }
    return json;
}